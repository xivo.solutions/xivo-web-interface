<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2016  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

dwho::load_class('dwho_network');

require_once(dwho_file::joinpath(XIVO_PATH_OBJECT,'abstract','libdatastorage.inc'));
require_once(dwho_file::joinpath(XIVO_PATH_OBJECT,'bus','publisher.inc'));

class xivo_ctistatus extends xivo_libdatastorage_abstract
{
    var $_name		= 'ctistatus';
    protected $_bus_msg_factory = null;

    public function __construct()
    {
        $this->_bus_msg_queue = new MessageQueue();
        $this->_bus_msg_factory = MessageFactory::from_config();
    }

    function reload()
    {
        $bus_msg = $this->_bus_msg_factory->ctistatus_reload_msg();
        $this->_bus_msg_queue->add_msg($bus_msg);
        return(true);
    }
}

?>

<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2014  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$array = array();

$array['element'] = array();

$array['element']['meetmeid'] = array();
$array['element']['meetmeid']['default'] = '';

$array['element']['name'] = array();
$array['element']['name']['default'] = '';

$array['element']['confno'] = array();
$array['element']['confno']['default'] = '';

$array['element']['context'] = array();
$array['element']['context']['default'] = 'default';

$array['element']['musiconhold'] = array();
$array['element']['musiconhold']['default'] = XIVO_SRE_IPBX_AST_MOH_DEFAULT;
$array['element']['musiconhold']['null'] = true;

$array['element']['user_pin'] = array();
$array['element']['user_pin']['default'] = '';
$array['element']['user_pin']['null'] = true;

$array['element']['description'] = array();
$array['element']['description']['default'] = '';

$array['element']['commented'] = array();
$array['element']['commented']['value'] = array(0,1);
$array['element']['commented']['default'] = 0;
$array['element']['commented']['set'] = true;
$array['element']['commented']['forcedef'] = true;

$array['element']['admin_pin'] = array();
$array['element']['admin_pin']['default'] = '';
$array['element']['admin_pin']['null'] = true;

$array['element']['user_waitingroom'] = array();
$array['element']['user_waitingroom']['value'] = array(0,1);
$array['element']['user_waitingroom']['default'] = 0;
$array['element']['user_waitingroom']['set'] = true;

$array['element']['noplaymsgfirstenter'] = array();
$array['element']['noplaymsgfirstenter']['value'] = array(0,1);
$array['element']['noplaymsgfirstenter']['default'] = 0;
$array['element']['noplaymsgfirstenter']['set'] = true;

$array['element']['quiet'] = array();
$array['element']['quiet']['value'] = array(0,1);
$array['element']['quiet']['default'] = 0;
$array['element']['quiet']['set'] = true;

$array['element']['announceusercount'] = array();
$array['element']['announceusercount']['value'] = array(0,1);
$array['element']['announceusercount']['default'] = 0;
$array['element']['announceusercount']['set'] = true;

$array['element']['user_announcejoinleave'] = array();
$array['element']['user_announcejoinleave']['value'] = array('no','yes','noreview');
$array['element']['user_announcejoinleave']['default'] = 'no';
$array['element']['user_announcejoinleave']['setdef'] = true;

$array['element']['record'] = array();
$array['element']['record']['value'] = array(0,1);
$array['element']['record']['default'] = 0;
$array['element']['record']['set'] = true;

$array['element']['maxusers'] = array();
$array['element']['maxusers']['default'] = 0;
$array['element']['maxusers']['set'] = true;

$array['element']['preprocess_subroutine'] = array();
$array['element']['preprocess_subroutine']['default'] = '';
$array['element']['preprocess_subroutine']['null'] = true;

dwho::load_class('dwho_network');

$array['filter'] = array();
$array['filter']['meetmeid'] = array('cast' => 'ulongint');
$array['filter']['name'] = array('regexp' => '/^[a-zA-Z0-9 _\.-]+$/','minlen' => 1,'maxlen' => 128);
$array['filter']['confno'] = array('set' => false,'chk' => 2,'regexp' => '/^[0-9\*#]+$/','minlen' => 1,'maxlen' => 40);
$array['filter']['context'] = array('callback' => 'xivo_service_asterisk::chk_context');
$array['filter']['musiconhold'] = array('set' => false,'chk' => 2,'cast' => 'sgraph','maxlen' => 20);
$array['filter']['user_pin'] = array('set' => false,'chk' => 2);
$array['filter']['description'] = array('set' => false,'chk' => 2,'maxlen' => 1000,'cast' => 'sgraph_crlf_tab','eol' => true);
$array['filter']['commented'] = array('set' => false,'chk' => 2);

$array['filter']['admin_pin'] = array('set' => false,'chk' => 2);
$array['filter']['user_waitingroom'] = array('bool' => true);

$array['filter']['noplaymsgfirstenter'] = array('bool' => true);
$array['filter']['quiet'] = array('bool' => true);
$array['filter']['announceusercount'] = array('bool' => true);
$array['filter']['user_announcejoinleave'] = array('set' => false,'chk' => 2,'key' => array('no','yes','noreview'));
$array['filter']['record'] = array('bool' => true);
$array['filter']['maxusers'] = array('set' => false,'chk' => 1,'cast' => 'uint','between' => array(0,99));
$array['filter']['preprocess_subroutine'] = array('set' => false,'chk' => 2,'callback' => 'xivo_service_asterisk::chk_context');

?>

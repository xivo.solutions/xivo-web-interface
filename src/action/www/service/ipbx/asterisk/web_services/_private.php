<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2014  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

function cidr_match($ip, $cidr) {
    if (strpos($cidr, '/') == false) {
        $subnet = $cidr;
        $mask = '32';
    } else {
        list($subnet, $mask) = explode('/', $cidr);
    }
    return ((ip2long($ip) & ~((1 << (32 - $mask)) - 1) ) == ip2long($subnet));
}

if (
    isset($_SERVER['REMOTE_ADDR']) === false
    || (
        $_SERVER['REMOTE_ADDR'] !== '127.0.0.1'
        && $_SERVER['REMOTE_ADDR'] !== '::1'
        && cidr_match($_SERVER['REMOTE_ADDR'], $_ENV['DOCKER_NET']) === false
       )
   )
{
	$http_response->set_status_line(403);
	$http_response->send(true);
}

?>

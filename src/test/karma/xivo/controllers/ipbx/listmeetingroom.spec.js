describe('listmeetingroom controller', () => {
  var $controller;
  var $rootScope;
  
  var scope;
  var ctrl;
  
  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  
  beforeEach(angular.mock.inject(function(_$controller_,_$rootScope_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    
    scope = $rootScope.$new();
  }));
  
  it('Checks that the controller exists', () => {
    ctrl = $controller('ListMeetingRoomsController', {'$scope': scope});
    expect(ctrl).toBeDefined();
  });
  
  it('toggle selected id when asked to', () => {
    $rootScope.selectedIds = [1, 3, 4];
    ctrl.toggleBox(3, $rootScope.selectedIds);
    expect($rootScope.selectedIds).toEqual([1,4]);
    ctrl.toggleBox(5, $rootScope.selectedIds);
    expect($rootScope.selectedIds).toEqual([1,4,5]);
  });
  
});

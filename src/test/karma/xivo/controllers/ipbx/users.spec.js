describe('ipbx-users controller', () => {
  var $controller;
  var $rootScope;
  var $q;
  
  var scope;
  var ctrl;
  var route;
  
  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  
  beforeEach(angular.mock.inject(function(_$q_, _$controller_,_$rootScope_, _route_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $q = _$q_;
    
    scope = $rootScope.$new();
    route = _route_;
  }));
  
  it('Checks that the controller exists', () => {
    ctrl = $controller('IpbxUsersController', {'$scope': scope});
    expect(ctrl).toBeDefined();
  });

  it('toggle selected id when asked to', () => {
    $rootScope.selectedIds = [1, 3, 4];
    ctrl.toggleBox(3, $rootScope.selectedIds);
    expect($rootScope.selectedIds).toEqual([1,4]);
    ctrl.toggleBox(5, $rootScope.selectedIds);
    expect($rootScope.selectedIds).toEqual([1,4,5]);
  });
  
});

describe('list-route controller', () => {
  var $rootScope;
  var $controller;

  var scope;
  var ctrl;
  var route;
  var $q;



  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _route_, _$q_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    route = _route_;
    $q = _$q_;

    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
  }));

  it('Checks that the controller exists', () => {
    ctrl = $controller('IpbxListRouteController', {
      '$scope': scope,
      'route' : route
    });
    expect(ctrl).toBeDefined();
  });

  it('Checks if the list of headers is defnined', () => {
    ctrl = $controller('IpbxListRouteController', {
      '$scope': scope,
      'route' : route
    });
    var headers = ['priority', 'dialpattern', 'context', 'mediaserver', 'trunk', 'description'];
    expect(scope.headers).toEqual(headers);
  });

  it('calls listRoute when controller is instantiated', () => {
    spyOn(route, 'listRoutes').and.callFake(() => {
      let defer = $q.defer();
      defer.resolve();
      return defer.promise;
    });

    ctrl = $controller('IpbxListRouteController', {
      '$scope': scope,
      'route' : route
    });


    $rootScope.$digest();
    expect(route.listRoutes).toHaveBeenCalled();
  });
});

describe('route service', () => {
  var $httpBackend;
  var route;

  var url = '/configmgt/api/1.0/route/';

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject((_route_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    route = _route_;
    $httpBackend.whenPOST('/configmgt/api/1.0/route/').respond('');
    $httpBackend.whenPUT('/configmgt/api/1.0/route/').respond('');
    $httpBackend.whenDELETE('/configmgt/api/1.0/route/1').respond('');
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('gets list of routes', () => {
    let response = [{
      "id": 1,
      "priority": 1,
      "internal": false,
      "dialpattern": [{"pattern":"+33X."}, {"pattern":"+420X."}, {"pattern":"XXXX"}],
      "context": [
        { "name": "Default", "used": true },
        { "name": "Another", "used": true }],
      "mediaserver": [
        { "id": 1, "name": "default", "display_name": "MDS 0", "used": true },
        { "id": 2, "name": "mds1", "display_name": "MDS 1", "used": false }],
      "trunk": [
        { "id": 1, "name": "MyTrunk", "used": true },
        { "id": 2, "name": "AnotherTrunk", "used": false }],
      "description": "",
    }];


    let resultAwaited = [{
      "id": 1,
      "priority": 1,
      "internal": false,
      "dialpattern": ["+33X.", "+420X.", "XXXX"],
      "context": ["Default","Another"],
      "mediaserver": ["MDS 0"],
      "trunk": ["MyTrunk"],
      "description": "",
    }];

    $httpBackend.expectGET(url).respond(() => {
      return [200, response];
    });
    let promise = route.listRoutes();
    $httpBackend.flush();

    promise.then((result) => {
      expect(result).toEqual(resultAwaited);
    });
  });

  it('checks validity of a route', () => {
    let myRoute = {
      "id": 1,
      "priority": 1,
      "internal": false,
      "dialpattern": [{id: 1, pattern: null}],
      "context": [],
      "mediaserver": [],
      "trunk": [],
      "description": null,
    };

    let errors = route.checkRoute(myRoute);
    expect(errors).toEqual(['dialpattern', 'trunk']);
  });

  it('adds a route', () => {
    let myFormRoute = {
      "priority": 1,
      "internal": false,
      "dialpattern": [{id: 1, pattern: "+33X."}, {id:2, pattern:"+420X.", target:"1234"}],
      "context": [
        { "name": "Default", "used": false },
        { "name": "Another", "used": false }],
      "mediaserver": [
        { "id": 2, "name": "mds1", "display_name": "MDS 1", "used": false }],
      "trunk": [
        { "id": 2, "name": "AnotherTrunk", "used": false }],
      "description": "desc"
    };

    let requestAwaited = {
      "priority": 1,
      "internal": false,
      "dialpattern": [{"pattern":"+33X."}, {"pattern":"+420X.", "target":"1234"}],
      "context": [
        { "name": "Default", "used": true },
        { "name": "Another", "used": true }],
      "mediaserver": [
        { "id": 2, "name": "mds1", "display_name": "MDS 1", "used": true }],
      "trunk": [
        { "id": 2, "name": "AnotherTrunk", "used": true }],
      "description": "desc"
    };

    route.createRoute(myFormRoute);
    $httpBackend.expectPOST('/configmgt/api/1.0/route/', requestAwaited);
  });

  it('updates a route', () => {
    let myOldRoute = {
      "id": 1,
      "priority": 1,
      "internal": false,
      "dialpattern": [{id: 1, pattern: "+33X.", target:"1234"}, {id:2, pattern:"+420X."}],
      "context": [
        { "name": "Default", "used": true }],
      "mediaserver": [
        { "id": 2, "name": "mds1", "display_name": "MDS 1", "used": true }],
      "trunk": [
        { "id": 2, "name": "AnotherTrunk", "used": true }],
      "description": "desc"
    };

    let myNewRoute = {
      "id": 1,
      "priority": 1,
      "internal": true,
      "dialpattern": [{id: 1, pattern: "+33X.", target:"", regexp: null}],
      "context": [
        { "name": "Another", "used": false }],
      "mediaserver": [],
      "trunk": [
        { "id": 2, "name": "AnotherTrunk", "used": true }],
      "description": "newDesc"
    };

    let requestAwaited = {
      "id": 1,
      "priority": 1,
      "internal": true,
      "dialpattern": [{"pattern":"+33X."}],
      "context": [
        { "name": "Another", "used": true }],
      "mediaserver": [],
      "trunk": [
        { "id": 2, "name": "AnotherTrunk", "used": true }],
      "description": "newDesc"
    };

    route.updateRoute(myOldRoute, myNewRoute);
    $httpBackend.expectPUT('/configmgt/api/1.0/route/', requestAwaited);
  });



  it('deletes a route', () => {
    route.deleteRoute(1);
    $httpBackend.expectDELETE('/configmgt/api/1.0/route/1');
  });

});

describe('directories-definitions-helper directive', () => {
  var $compile;
  var $rootScope;
  var $window;
  var isolatedScope;
  var $translate;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_, _$window_, _$translate_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $window = _$window_;
    $translate = _$translate_;
  }));

  const genDirective = () => {
    let elem = angular.element(`<directories-definitions-helper></directories-definitions-helper>`);
    scope = $rootScope.$new();

    let elemCompiled = $compile(elem)(scope);
    $rootScope.$digest();

    let scope = elemCompiled.isolateScope();
    return scope;
  };

  it('should init directive', () => {
    isolatedScope = genDirective();
    expect(isolatedScope).toBeDefined;
  });

});

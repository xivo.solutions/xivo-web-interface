describe('password-policy-valid directive', () => {
  var $compile;
  var $rootScope;
  var isolatedScope;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  const genDirective = (regex, password = "") => {
    let elem = angular.element(`<password-policy-valid password="'${password}'"></password-policy-valid>`);
    scope = $rootScope.$new();

    let elemCompiled = $compile(elem)(scope);
    $rootScope.$digest();

    let scope = elemCompiled.isolateScope();
    scope.validPolicy = new RegExp(regex);
    return scope;
  };

  it('regex should not work without letters/digit/upper/lower/special char', () => {
    let regex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*?[0-9])(?=.*[\\W_])[A-Za-z0-9\\W_][^\"\\\'\<\>]{14,}$";

    isolatedScope = genDirective(regex, "nodigits");
    expect(isolatedScope.passwordPolicyValid()
    ).toEqual(false);

    isolatedScope = genDirective(regex, "withdigits123");
    expect(isolatedScope.passwordPolicyValid()
    ).toEqual(false);

    isolatedScope = genDirective(regex, "nodigitswithspecialchar!$.");
    expect(isolatedScope.passwordPolicyValid()
    ).toEqual(false);

    isolatedScope = genDirective(regex, "NODIGITONLYUPPER");
    expect(isolatedScope.passwordPolicyValid()
    ).toEqual(false);

    isolatedScope = genDirective(regex, "withUpperAndDigits1254");
    expect(isolatedScope.passwordPolicyValid()
    ).toEqual(false);
  });

  it('removes forbidden characters to prevent form breaking html', () => {
    let faultyPassword = `2@0dRt"D9s2'<Lkv)c!tOwRhQ><&L$`;
    let expectedpassword = "2@0dRtD9s2Lkv)c!tOwRhQ&L$";
    isolatedScope.sanitize(faultyPassword);
    expect(isolatedScope.password).toEqual(expectedpassword);
  });

  it('regex should work with a correct password', () => {
    let regex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*?[0-9])(?=.*[\\W_])[A-Za-z0-9\\W_][^\"\\\'\<\>]{14,}$";

    isolatedScope = genDirective(regex, "2@0dRtD9s2Lkv)c!tOwRhQ&L$");
    expect(isolatedScope.passwordPolicyValid()
    ).toEqual(true);
  });

  it('should generate a valid password', () => {
    let regex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*?[0-9])(?=.*[\\W_])[A-Za-z0-9\\W_][^\"\\\'\<\>]{14,}$";
    isolatedScope = genDirective(regex);

    spyOn(isolatedScope, "togglePasswordVisibility").and.callFake(() => {});
    isolatedScope.genPass();
    expect(isolatedScope.password).toMatch(isolatedScope.validPolicy);
  });
});
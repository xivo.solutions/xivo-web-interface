<div class="b-infos b-form container-fluid">
  <breadcrumb parent="{{'meetingroom' | translate}}" page="{{'add' | translate}}"></breadcrumb>
  <div ng-controller="AddMeetingroomController as ctrl">

    <supertoolbar toolbar-error="toolbarError"></supertoolbar>
    <form action="" class="form-horizontal" accept-charser="utf-8" name="addMeetingroomForm">

          <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : addMeetingroomForm.displayName.$invalid && !addMeetingroomForm.displayName.$pristine }">
            <div class="row">
              <div class="col-sm-1"></div>
              <label class="control-meetingroom col-sm-2">{{'display_name' | translate}}</label>
              <div class="col-sm-3">
                <input type="text" name="displayName" ng-keyup="ctrl.checkDisplayNameAvailability(addMeetingroomForm)" class="form-control" size=30 ng-model="form.displayName" required>
              </div>
              <div class="col-sm-4">
                <div ng-messages="addMeetingroomForm.displayName.$error">
                  <div ng-message="duplicatedisplayName" class="fm-error-icon">{{'name_duplicate' | translate}}</div>
                  <div ng-show="!addMeetingroomForm.displayName.$pristine" ng-message="required" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
                </div>
              </div>
              <div class="col-sm-2"></div>
          </div>
        </div>

        <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : addMeetingroomForm.number.$invalid && !addMeetingroomForm.number.$pristine }">
          <div class="row">
              <div class="col-sm-1"></div>
              <label class="control-meetingroom col-sm-2">{{'room_number' | translate}}</label>
              <div class="col-sm-3">
                  <input type="text" name="number" ng-keyup="ctrl.checkNumberAvailability(addMeetingroomForm)" class="form-control" ng-pattern="ctrl.number_validation" size=30 ng-model="form.number" required>
              </div>
              <div class="col-sm-4">
                <div ng-messages="addMeetingroomForm.number.$error">
                  <div ng-message="pattern" class="fm-error-icon">{{'number_validation' | translate}}</div>
                  <div ng-message="duplicatenumber" class="fm-error-icon">{{'number_duplicate' | translate}}</div>
                  <div ng-show="!addMeetingroomForm.number.$pristine" ng-message="required" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
                </div>
              </div>
              <div class="col-sm-2"></div>
          </div>
        </div>

        <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : addMeetingroomForm.userPin.$invalid && addMeetingroomForm.userPin.length > 0 }">
          <div class="row">
              <div class="col-sm-1"></div>
              <label class="control-meetingroom col-sm-2">{{'room_pincode' | translate}}</label>
              <div class="col-sm-3">
                  <input type="text" name="userPin" class="form-control" ng-pattern="ctrl.number_validation" size=30 ng-model="form.userPin">
              </div>
              <div class="col-sm-4">
                <div ng-messages="addMeetingroomForm.userPin.$error">
                  <div ng-message="pattern" class="fm-error-icon">{{'number_validation' | translate}}</div>
                </div>
              </div>
              <div class="col-sm-2"></div>
          </div>
        </div>

      <p class="fm-paragraph-submit">
        <input ng-click="ctrl.submit()" ng-disabled="!addMeetingroomForm.$valid || ctrl.isLoading" type="button" name="submit" id="it-submit" class="btn btn-primary it-submit" value="{{'fm_bt-save' | translate}}">
      </p>
    </form>
  </div>
</div>

<div ng-controller="ListLabelsController as ctrl">
	<supertoolbar plus-action="ctrl.gotoAddLabelPage" dropdown-actions="ctrl.supertoolbarDropdownActions" toolbar-error="toolbarError"></supertoolbar>
    <div class="pagination-no-top">
        <div ng-table-pagination="usersTable" template-url="'ng-table/pager.html'"></div>
    </div>
    <table ng-table="tableParams" class="table table-condensed table-striped-reverse table-hover table-bordered" show-filter="true">
        <tr ng-repeat="row in $data  track by row.id">
            <td class="td-left">
                <div class="form-group form-group-sm form-inline">
                    <div class="col-sm-3">
                        <input type="checkbox" ng-click="ctrl.toggleBox(row.id, ctrl.selectedIds)" name="selectedIds[]" id="it-labels-{{row.id}}" class="it-checkbox" value="{{row.id}}">
                    </div>
                </div>
            </td>
            <td data-title="'display_name' | translate" filter="{display_name: 'text'}" sortable="'display_name'"><b>{{row.display_name || "-"}}</b></td>
            <td data-title="'description' | translate" filter="{description: 'text'}" sortable="'description'">{{(row.description) || "-"}}</td>
            <td data-title="'users_count' | translate" filter="{users_count: 'number'}" sortable="'users_count'">{{row.users_count || "-" }}</td>
            <td class="td-right" data-title="'Actions'">
                <span ng-repeat="action in actions" ng-switch="action" title="{{action | translate}}"
                      class="no-underline">
            <a ng-if="!row['read_only']" ng-switch-when="edit" ng-href="{{path}}?act=edit&id={{row.id}}">
              <img ng-src="/img/site/button/{{action}}.png" alt="{{action | translate}}"/>
            </a>
            <a ng-if="!row['read_only']" ng-switch-when="delete" ng-click="ctrl.delete(row.id)"
               ng-confirm-click="{{'confirm_delete_label' | translate}}" href="">
              <img ng-src="/img/site/button/{{action}}.png" alt="{{action | translate}}"/>
            </a>
          </span>
            </td>
        </tr>
        <tr ng-show="$data.length == 0">
            <td ng-if="isLoading" colspan="9" class="text-center">
                <i class="fa fa-spinner fa-pulse fa-fw"></i>
            </td>
            <td ng-if="!isLoading" colspan="9" class="empty">
                {{ 'no_labels_found' | translate }}
            </td>
        </tr>
    </table>
</div>
<div ng-controller="AddLabelsController as ctrl">
	<supertoolbar toolbar-error="toolbarError"></supertoolbar>
  <div class="b-infos b-form">
    <breadcrumb parent="{{'labels' | translate}}" page="{{'add' | translate}}"></breadcrumb>
      <form action="" class="form-horizontal" accept-charser="utf-8" name="createLabelForm">
        
        <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : createLabelForm.display_name.$invalid && !createLabelForm.display_name.$pristine }">
          <div class="row"> 
            <div class="col-sm-1"></div>
            <label for="it-display-name" class="control-label col-sm-2" id="lb-display-name">{{'display_name' | translate}}</label>
            
              <div class="col-sm-3">
                <input type="text" name="display_name" id="it-display-name" ng-pattern="ctrl.name_validation" class="form-control" size=30 ng-model="display_name" required>
              </div>
              <div class="col-sm-4">
                <div ng-messages="createLabelForm.display_name.$error">
                  <div ng-message="pattern" class="fm-error-icon">{{'name_validation' | translate}}</div>
                  <div ng-show="!createLabelForm.display_name.$pristine" ng-message="required" class="fm-error-icon">{{'fm_field_required' | translate}}</div> 
                </div>           
              </div>
              <div class="col-sm-2"></div> 
          </div>  
        </div>
        <div class="form-group form-group-sm form-inline labels-description" ng-class="{ 'has-error' : createLabelForm.display_name.$invalid && !createLabelForm.display_name.$pristine }">
          <div class="row"> 
            <div class="col-sm-1"></div>
            <label for="it-description" class="control-label col-sm-2" id="lb-description">{{'description' | translate}}</label>
            <div class="col-sm-9">
              <textarea name="description" id="it-description" placeholder="{{'description_labels' | translate}}" class="form-control labels-textarea-description" rows="4" ng-model="description"></textarea>
            </div>
          </div>
        </div>
        <p class="fm-paragraph-submit">
          <input ng-click="ctrl.validate()" ng-disabled="!createLabelForm.$valid" type="button" name="submit" id="it-submit" class="btn btn-primary it-submit" value="{{'fm_bt-save' | translate}}">
        </p>
      </form>
  </div>
</div>
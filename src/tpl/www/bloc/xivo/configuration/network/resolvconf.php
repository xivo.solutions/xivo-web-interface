<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2017  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$form = &$this->get_module('form');
$dhtml = &$this->get_module('dhtml');

$element = $this->get_var('element');

?>
<div class="b-infos b-form">
<breadcrumb
        page="<?=$this->bbf('title_content_name');?>">
</breadcrumb>

<div class="sb-content">
<form class="form-horizontal" action="#" method="post" accept-charset="utf-8">

<uib-tabset active="active">

	<uib-tab index="0" heading="<?=$this->bbf('smenu_general');?>">
		<?php
			echo	$form->hidden(array('name'	=> DWHO_SESS_NAME,
								'value'	=> DWHO_SESS_ID)),

				$form->hidden(array('name'	=> 'fm_send',
								'value'	=> 1)),

				$form->text(array('desc'	=> $this->bbf('fm_hostname'),
							'name'	=> 'hostname',
							'labelid'	=> 'hostname',
							'size'	=> 15,
							'default'	=> $element['resolvconf']['hostname']['default'],
							'value'	=> $this->get_var('info','hostname'),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'hostname')) )),

				$form->text(array('desc'	=> $this->bbf('fm_domain'),
							'name'	=> 'domain',
							'labelid'	=> 'domain',
							'size'	=> 15,
							'default'	=> $element['resolvconf']['domain']['default'],
							'value'	=> $this->get_var('info','domain'),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'domain')) )),

				$form->text(array('desc'	=> $this->bbf('fm_nameserver1'),
							'name'	=> 'nameserver1',
							'labelid'	=> 'nameserver1',
							'size'	=> 15,
							'default'	=> $element['resolvconf']['nameserver1']['default'],
							'value'	=> $this->get_var('info','nameserver1'),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'nameserver1')) )),

				$form->text(array('desc'	=> $this->bbf('fm_nameserver2'),
							'name'	=> 'nameserver2',
							'labelid'	=> 'nameserver2',
							'size'	=> 15,
							'default'	=> $element['resolvconf']['nameserver2']['default'],
							'value'	=> $this->get_var('info','nameserver2'),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'nameserver2')) )),

				$form->text(array('desc'	=> $this->bbf('fm_nameserver3'),
							'name'	=> 'nameserver3',
							'labelid'	=> 'nameserver3',
							'size'	=> 15,
							'default'	=> $element['resolvconf']['nameserver3']['default'],
							'value'	=> $this->get_var('info','nameserver3'),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'nameserver3')) ));
		?>
		<div class="fm-paragraph fm-description">
			<p>
				<label id="lb-description" for="it-description"><?=$this->bbf('fm_description');?></label>
			</p>
			<?=$form->textarea(array('paragraph'	=> false,
						'label'	=> false,
						'name'		=> 'description',
						'id'		=> 'it-description',
						'cols'		=> 60,
						'rows'		=> 5,
						'default'	=> $element['resolvconf']['description']['default'],
						'error'	=> $this->bbf_args('error',
							$this->get_var('error', 'description')) ),
						$this->get_var('info','description'));?>
		</div>
</uib-tab>

	<uib-tab index="1" heading="<?=$this->bbf('smenu_search');?>">
		<?php
			echo	$form->text(array('desc'	=> $this->bbf('fm_search1'),
							'name'	=> 'search[]',
							'labelid'	=> 'search1',
							'size'	=> 15,
							'value'	=> $this->get_var('search',0),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'search',0)) )),

				$form->text(array('desc'	=> $this->bbf('fm_search2'),
							'name'	=> 'search[]',
							'labelid'	=> 'search2',
							'size'	=> 15,
							'value'	=> $this->get_var('search',1),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'search',1)) )),

				$form->text(array('desc'	=> $this->bbf('fm_search3'),
							'name'	=> 'search[]',
							'labelid'	=> 'search3',
							'size'	=> 15,
							'value'	=> $this->get_var('search',2),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'search',2)) )),

				$form->text(array('desc'	=> $this->bbf('fm_search4'),
							'name'	=> 'search[]',
							'labelid'	=> 'search4',
							'size'	=> 15,
							'value'	=> $this->get_var('search',3),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'search',3)) )),

				$form->text(array('desc'	=> $this->bbf('fm_search5'),
							'name'	=> 'search[]',
							'labelid'	=> 'search5',
							'size'	=> 15,
							'value'	=> $this->get_var('search',4),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'search',4)) )),

				$form->text(array('desc'	=> $this->bbf('fm_search6'),
							'name'	=> 'search[]',
							'labelid'	=> 'search6',
							'size'	=> 15,
							'value'	=> $this->get_var('search',5),
							'error'	=> $this->bbf_args('error',
								$this->get_var('error', 'name',5)) ));
		?>
	</uib-tab>
</uib-tabset>

<?php

echo	$form->submit(array('name'	=> 'submit',
			    'id'	=> 'it-submit',
			    'value'	=> $this->bbf('fm_bt-save')));

?>
</form>
	</div>
	<div class="sb-foot xspan"></div>

<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2016  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$form = &$this->get_module('form');
$url = &$this->get_module('url');

$element = $this->get_var('element');
$info = $this->get_var('info');

$data = $this->get_var('data');
$urilist = $this->get_var('urilist');
$presence = $this->get_var('displays');

if ($info["displays"]["deletable"] == 0 && $_GET["act"] == "edit") {
	$default_filter = true;
} else {
	$default_filter = false;
};

function build_row($data, $form, $url, $helper, $default_filter) {
	$row = '<tr class="fm-paragraph"><td>';
	$row .= $form->text(
		array(
			'paragraph' => false,
			'label' => false,
			'key' => false,
			'size' => 15,
			'name' => 'dispcol1[]',
			'value' => $data[0],
		)
	);
	$row .= "</td><td>";

	$row .= $form->text(
		array(
			'paragraph' => false,
			'label' => false,
			'key' => false,
			'size' => 15,
			'name' => 'dispcol2[]',
			'value' => $data[1],
			'uneditable' => $default_filter,
			'class' => 'field-type-name form-control',
		)
	);
	$row .= "</td><td>";
	

	if (!$default_filter) {
		$row .= $form->text(
			array(
				'paragraph' => false,
				'label' => false,
				'key' => false,
				'size' => 15,
				'name' => 'dispcol3[]',
				'value' => $data[2],
			)
		);
		$row .= "</td><td>";
	}; 

	$row .= $form->text(
		array(
			'paragraph' => false,
			'label' => false,
			'key' => false,
			'size' => 15,
			'name' => 'dispcol4[]',
			'value' => $data[3],
			'uneditable' => $default_filter,
		)
	);
	
	$row .= '</td><td class="td-right">';
	if (! $default_filter) {
		$row .= $url->href_html(
			$url->img_html(
				'img/site/button/mini/blue/delete.png',
				$helper->bbf('opt_disp-delete'),
				'border="0"'
			),
			'#',
			null,
			null,
			$helper->bbf('opt_disp-delete'),
			false,
			'&amp;',
			true,
			true,
			true,
			true,
			'display-filter-remove'
		);
		$row .= "</td>";
	};

	$row .= "</tr>";

	return $row;
}

?>

<div id="sb-part-first">
<?php
	if ($default_filter) {
		echo	$form->text(array('desc'	=> $this->bbf('fm_displays_name'),
				  'name'	=> 'displays[name]',
				  'labelid'	=> 'displays-name',
				  'size'	=> 30,
				  'default'	=> $element['displays']['name']['default'],
				  'uneditable' => true,
				  'value'	=> $info['displays']['name']));
	} else {
		echo	$form->text(array('desc'	=> $this->bbf('fm_displays_name'),
				  'name'	=> 'displays[name]',
				  'labelid'	=> 'displays-name',
				  'size'	=> 30,
				  'default'	=> $element['displays']['name']['default'],
				  'value'	=> $info['displays']['name']));
	}
	

?>
	<p>&nbsp;</p>
	<div class="sb-list">
		<table class="table table-condensed table-hover">
			<thead>
			<tr class="sb-top">

				<th class="th-left"><?=$this->bbf('col_1');?></th>
				<th class="th-center"><?=$this->bbf('col_2');?></th>
			<?php if (!$default_filter) { ?>
				<th class="th-center"><?=$this->bbf('col_3');?></th>
			<?php }; ?>
				<th class="th-center"><?=$this->bbf('col_4');?></th>
			<?php if (!$default_filter) { ?>
				<th class="th-right">
					<?=$url->href_html($url->img_html('img/site/button/mini/orange/bo-add.png',
									  $this->bbf('col_add'),
									  'border="0"'),
							   '#',
							   null,
							   'id="display-filter-add"',
							   $this->bbf('col_add'));?>
				</th>
			<?php }; ?>
			</tr>
			</thead>
			<tbody id="disp">
					<?php foreach($data as $data_row): ?>
						<?= build_row($data_row, $form, $url, $this, $default_filter) ?>
					<?php endforeach ?>
			</tbody>
		</table>

<script type='text/javascript'>

var displayFilterRow = <?= dwho_json::encode(build_row(array('', '', '', ''),$form, $url, $this,$default_filter)) ?>;

var fieldTypes = [
		"agent",
		"callable",
		"email",
		"favorite",
		"name",
		"number",
		"personal",
		"voicemail",
];

function attachEvents(row) {
	option = row.find(".field-type-name");
    option.autocomplete({
      source: fieldTypes,
      minLength: 0
    });
    option.focus(function() {
        $(this).autocomplete("search", "");
    });

	remove = row.find(".display-filter-remove");
	remove.click(function(e) {
		e.preventDefault();
		row.detach();
	});
}

$(function() {
	$("#display-filter-add").click(function(e) {
		e.preventDefault();
		$("#disp").append(displayFilterRow);
		row = $("#disp tr:last");
		attachEvents(row);
	});

	$("#disp tr").each(function(pos, row) {
		attachEvents($(row));
	});
});

</script>
	</div>
<br />
<div class="col-sm-offset-2 fm-paragraph fm-description">
	<p>
		<label id="lb-description" for="it-description"><?=$this->bbf('fm_description');?></label>
	</p>
	<?=$form->textarea(array('paragraph'    => false,
				 'label'    => false,
				 'name'     => 'displays[description]',
				 'id'       => 'it-description',
				 'cols'     => 60,
				 'rows'     => 5,
				 'default'  => $element['displays']['description']['default']),
			   $info['displays']['description']);?>
</div>

</div>

<div class="col-sm-offset-2 fm-paragraph fm-description"><p><?=$this->bbf('need-xivo-dird-restart');?></p></div>

<? if ($default_filter) { ?>
	<script>
		var table = document.querySelector("table")
		
		var headerRow = table.insertRow(1)
		headerRow.innerHTML = "<span><?=$this->bbf('headers');?></span>"
		headerRow.className = "bold-line"

		var contactsRow = table.insertRow(5)
		contactsRow.innerHTML = "<span><?=$this->bbf('contacts');?></span>"
		contactsRow.className = "bold-line"

		var generalRow = table.insertRow(12)
		generalRow.innerHTML = "<span><?=$this->bbf('general');?></span>"
		generalRow.className = "bold-line"

		var locationRow = table.insertRow(18)
		locationRow.innerHTML = "<span><?=$this->bbf('location');?></span>"
		locationRow.className = "bold-line"

		var extraRow = table.insertRow(21)
		extraRow.innerHTML = "<span><?=$this->bbf('others');?></span>"
		extraRow.className = "bold-line"

	</script>
<? }; ?>
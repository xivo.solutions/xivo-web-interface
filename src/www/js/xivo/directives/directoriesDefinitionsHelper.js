export default function directoriesDefinitionsHelper($window, $translate) {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/directoriesDefinitionsHelper.html',
    scope: {

    },
    link(scope) {
      scope.showHelper = () => {
        let helperWindow = $window.open("", "Contact", "width=500,height=950,popup,resizable=0");
        if (helperWindow.document.querySelector("img") == undefined) helperWindow.document.write(`<img src="/img/site/utils/sheet_model.png">`);
        else helperWindow.focus();
      };

      scope.fieldNameFields = [
        'header_1_info',
        'header_2_info',
        'header_3_info',
        'contact_1_callable',
        'contact_2_callable',
        'contact_3_callable',
        'contact_4_callable',
        'contact_5_email',
        'contact_6_callable',
        'general_1_info',
        'general_2_info',
        'general_3_info',
        'general_4_info',
        'general_5_info',
        'location_1_info',
        'location_2_info',
        'picture',
        'reverse'
      ];

      scope.init = () => {
        scope.attachEvents();
        scope.handleWarn();
      };

      scope.attachEvents = () => {
        function attachEvents(row) {
          let option = row.find(".field-name");
          option.autocomplete({
            source: scope.fieldNameFields,
            minLength: 0,
            change: function (_e, _ui) {
              scope.handleWarn();
            },
            close: function (_e, _ui) {
              scope.handleWarn();
            }
          });
          option.focus(function() {
            $(this).autocomplete("search", "");
          });
          option.on("keyup", () => {
            scope.handleWarn();
          });

          let remove = row.find(".directories-definitions-delete");
          remove.click(function(e) {
            e.preventDefault();
            row.detach();
            scope.handleWarn();
          });
        }

        $(function() {
          $("#directories-definitions-add").click(function(e) {
            e.preventDefault();
            let row = $("#disp tr:last");
            row.find("input:first").addClass("field-name");
            attachEvents(row);
          });

          $("#disp tr").each(function(pos, row) {
            attachEvents($(row));
          });
        });
      };

      scope.handleWarn = () => {
        let inputs = [];
        $(".field-name").each((index, elem) => {
          inputs.push({id: index + 1, name: elem.value});
        });
        scope.checkUnrecognizedFields(inputs);
      };

      scope.checkUnrecognizedFields = (array) => {
        if (array && array.length > 0) {
          array.forEach(field => scope.fieldNameFields.includes(field.name) ? scope.removeWarnColorOnInput(field.id) : scope.setWarnColorOnInput(field.id));
          array.find(field => !scope.fieldNameFields.includes(field.name)) != undefined ? scope.addWarnUnknownFieldMsg() : scope.removeWarnUnknownFieldMsg();
        } else scope.removeWarnUnknownFieldMsg();
      };

      scope.addWarnUnknownFieldMsg = () => {
        $translate('warn_unknown_field').then((msg) => $("#warn-div").html(`<div class="alert alert-warning font-italic">${msg}</div>`)).catch((e)=>console.debug(e));
      };

      scope.removeWarnUnknownFieldMsg = () => {
        $("#warn-div").empty();
      };

      scope.setWarnColorOnInput = (fieldId) => {
        let row = $(`#disp tr:nth-child(${fieldId})`);
        let input = row.find("input:first");
        input.css("border", "2px solid #ffc641");
      };

      scope.removeWarnColorOnInput = (fieldId) => {
        let row = $(`#disp tr:nth-child(${fieldId})`);
        let input = row.find("input:first");
        input.css("border", "1px solid #c6c6c6");
      };

      scope.init();
    }
  };
}

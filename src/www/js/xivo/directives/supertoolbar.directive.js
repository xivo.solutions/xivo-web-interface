export default function supertoolbar() {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/supertoolbar.html',
    scope: {
      plusAction: "=?",
      dropdownActions: '=?',
      toolbarError: '='
    },
    link(scope) {

      scope.$watch('toolbarError', (newValue, oldValue) => {
        if (newValue === oldValue) return;
        scope.displayedError = newValue;
      });
      scope.openDropdown = false;
    }
  };
}

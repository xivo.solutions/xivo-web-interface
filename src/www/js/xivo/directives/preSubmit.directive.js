export default function preSubmit($http, $log, submitForm) {

  return function(scope, element) {
    element.bind("submit", (event) => {
      if (!submitForm.isValidated()) {
        event.preventDefault();
        submitForm.notify(angular.element(element[0]));
      }
    });
  };
}

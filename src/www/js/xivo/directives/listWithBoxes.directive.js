export default function listWithBoxes() {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/list-with-boxes.html',
    scope: {
      items: '=',
      checkboxToggleAction: '&',
      precheckedBoxes: '=?',
      listTitle: '=',
      onEmptyList: '='
    },
    link(scope) {
      
      scope.$watch('items', (newValue, oldValue) => {
        if (newValue === oldValue) return;
        scope.items = newValue;
        if (newValue.length > 5) {
          let listwithboxes = angular.element('.listwithboxes-body-list-wrapper');
          listwithboxes.addClass('listwithboxes-scrollable');
        }
      });

    }
  };
}

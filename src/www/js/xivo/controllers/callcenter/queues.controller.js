/* global clean_ms */

export default class CCQueuesController {

  constructor($http, $log, $window, $scope, toolbar, queues, submitForm) {
    this.$http = $http;
    this.$log = $log;
    this.submitForm = submitForm;
    this.queues = queues;
    this.toolbar = toolbar;
    this.recording = {};

    this.submitForm.subscribe($scope, this.onSubmit.bind(this));
    this.init_tab_announce_done = false;

    let params = toolbar.parseParams($window.location.search);
    this.queueId = params.id;

    if (params.act == 'edit') {
      this.queues.getRecConfig(this.queueId).then((response) => {
        this.recording = response.data;
      }, (error) => {
        this.toolbar.setError('fm_generic_error');
        $log.error(error);
      });
    }
  }

  onSubmit() {
    this.queues.setRecConfig(this.queueId, this.recording).then(() => {
      this.submitForm.validate();
      this.submitForm.post();
    }, (error) => {
      this.$log.error(error);
      if(error.status === 403) {
        this.toolbar.setError('fm_recording_error_rights');
      } else {
        this.toolbar.setError('fm_recording_error');
      }
    });
  }

  init_tab_announce() {
    if (!this.init_tab_announce_done) {
      this.init_tab_announce_done = new clean_ms('it-pannouncelist-finder','it-pannouncelist','it-queue-periodic-announce').__init();
    }
  }
}

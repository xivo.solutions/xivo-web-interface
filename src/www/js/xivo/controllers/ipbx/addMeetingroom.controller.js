export default class AddMeetingroomController {
  constructor(meetingRoom, $scope, $window, $log) {
    this.meetingRoom = meetingRoom;
    this.$scope = $scope;
    this.$window = $window;
    this.$log = $log;
    this.number_validation = /^[0-9]+$/;
    this.$scope.form = {};
    this.$scope.nameAvailable = true;
    this.$scope.numberAvailable = true;
    this.isLoading = false;
    this.submit = this.submit.bind(this);
  }

  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }

  async submit () {
    this.meetingRoom.createMeetingRoom(
      new this.meetingRoom.MeetingRoom(
        undefined,
        this.$scope.form.displayName,
        this.$scope.form.number,
        this.$scope.form.userPin
      )
    ).then(() => {
      this.$window.location.href = '?act=list';
    }, (error) => {
      this.onError(error, 'fm_mgt_error');
    });
  }

  checkNumberAvailability (form) {
    this.checkAvailability('number', form, this.meetingRoom.numberIsAvailable);
  }

  checkDisplayNameAvailability (form) {
    this.checkAvailability('displayName', form, this.meetingRoom.displayNameIsAvailable);
  }

  checkAvailability (name, form, fn) {
    if (this.$scope.form[name]) {
      this.isLoading = true;
      fn(this.$scope.form[name]).then((result) => {
        form[name].$setValidity(`duplicate${name}`, result);
        this.isLoading = false;
      });
    }
  }
}

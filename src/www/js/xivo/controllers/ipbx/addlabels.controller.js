export default class AddLabelsController {
  constructor(userLabels, $scope, $window, $log) {
    this.userLabels = userLabels;
    this.$scope = $scope;
    this.$window = $window;
    this.$log = $log;
    this.name_validation = /^[A-Za-z0-9_-\s]+$/;
    this.validate = this.validate.bind(this);


    let nameInput = angular.element('#it-display-name')[0];
    if (nameInput){
      nameInput.addEventListener("keydown", function(event) {
        if (event.keyCode == 13) {
          event.preventDefault();
        }
      });
    }
  }
  
  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }

  async validate() {
    this.userLabels.createLabel(await this.userLabels.auth(), {
      "display_name": this.$scope.display_name,
      "description": this.$scope.description || ""
    }).then(() => {
      this.$window.location.href = '?act=list';
    }, (error) => {
      if ( error.data && error.data[0].includes('Label already exists')) {
        this.onError(error, 'label_duplicate_error');
      } else {
        this.onError(error, 'fm_confd_error');
      }
    });
  }
}

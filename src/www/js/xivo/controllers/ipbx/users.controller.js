/* global clean_ms */

let $ = require('jquery');

export default class IpbxUsersController {

  constructor(userLabels, $scope, $log) {
    this.userLabels = userLabels;
    this.$scope = $scope;
    this.$log = $log;

    this.init();
  }

  init_tab_group() {
    if (!this.init_tab_group_done) {
      new clean_ms('it-grouplist-finder', 'it-grouplist', 'it-group').__init();
      new clean_ms('it-queuelist-finder', 'it-queuelist', 'it-queue').__init();
      this.init_tab_group_done = true;
    }
  }

  handleError(error) {
    this.$log.error(error);
    this.$scope.error = "fm_generic_error";
  }

  async listLabels() {
    return this.userLabels.listLabels(await this.userLabels.auth())
            .then(result => {
              return result.data.items;
            })
            .catch(error => {
              this.handleError(error);
              return [];
            });
  }

  toggleBox(id, ids) {
    ids.includes(id) ? ids.splice(ids.indexOf(id), 1) : ids.push(id);
  }

  async init() {
    this.init_tab_group_done = false;
    this.$scope.labels = await this.listLabels();
    this.selectedIds = [...window.userLabels] || [];
    this.$scope.$digest();
    let selector = 'select[name=labelsSelect]';
    $.fn.selectpicker.Constructor.BootstrapVersion = '3';
    angular.element(document).ready(function () {
      $(selector).selectpicker("destroy");
      $(selector).selectpicker("render");
    });
    $(selector).val(this.selectedIds);
    $(selector).selectpicker('refresh');
  }
}

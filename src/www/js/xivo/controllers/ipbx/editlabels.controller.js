export default class EditLabelsController {
  constructor(userLabels, $scope, $window, $log) {
    this.userLabels = userLabels;
    this.$scope = $scope;
    this.$window = $window;
    this.$log = $log;
    this.name_validation = /^[A-Za-z0-9_-\s]+$/;
    this.validate = this.validate.bind(this);

    let nameInput = angular.element('#it-display-name')[0];
    if (nameInput) {
      nameInput.addEventListener("keydown", function(event) {
        if (event.keyCode == 13) {
          event.preventDefault();
        }
      });
    }

    this.init();
  }

  parseParams (search) {
    return (search).replace(/(^\?)/,'').split("&").reduce((p,n) => {
      return n = n.split("="), p[n[0]] = n[1], p;
    }, {});
  }

  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }

  async validate () {
    this.userLabels.updateLabel(await this.userLabels.auth(), {
      "id": +this.params.id,
      "display_name": this.$scope.label.display_name,
      "description": this.$scope.label.description || "", 
    }, this.params.id).then(() => {
      this.$window.location.href = '?act=list';
    }, (error) => {
      if (error.data.error === 'Duplicate') {
        this.onError(error, 'label_duplicate_error');
      } else {
        this.onError(error, 'fm_confd_error');
      }
    });
  }

  async init () {
    this.params = this.parseParams(this.$window.location.search);
    this.userLabels.getLabel(await this.userLabels.auth(), this.params.id)
    .then((result) => {
      this.$scope.label = result.data;
    }, (error) => {
      this.onError(error, 'fm_confd_error');
    });
  }
}

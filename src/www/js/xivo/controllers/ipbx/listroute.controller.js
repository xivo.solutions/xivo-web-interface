export default class IpbxListRouteController {
  constructor($scope, $log, $window, route, toolbar) {
    this.$scope = $scope;
    this.$log = $log;
    this.$window = $window;
    this.toolbar = toolbar;

    this.$scope.list = [];

    this.$scope.headers = [
      'priority',
      'dialpattern',
      'context',
      'mediaserver',
      'trunk',
      'description'
    ];
    this.$scope.actions = [
      'edit',
      'delete'
    ];
    this.$scope.path = '/service/ipbx/index.php/call_management/route/';
    this.$scope.delete = (id) => {
      route.deleteRoute(id).then(() => {
        this.$window.location.href = '?act=list';
      }, (error) => {
        this.toolbar.setError('fm_generic_error');
        $log.error(error);
      });
    };

    this.$scope.isLoading = true;
    route.listRoutes().then((response) => {
      this.$scope.list = response;
      this.$scope.isLoading = false;
    }, (error) => {
      this.toolbar.setError('fm_generic_error');
      $log.error(error);
    });
  }
}

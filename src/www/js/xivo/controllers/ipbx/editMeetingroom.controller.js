export default class EditMeetingroomController {
  constructor(meetingRoom, $scope, $window, $log) {
    this.meetingRoom = meetingRoom;
    this.$scope = $scope;
    this.$window = $window;
    this.$log = $log;
    this.number_validation = /^[0-9]+$/;
    this.submit = this.submit.bind(this);

    this.init();
  }

  parseParams (search) {
    return (search).replace(/(^\?)/,'').split("&").reduce((p,n) => {
      return n = n.split("="), p[n[0]] = n[1], p;
    }, {});
  }

  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }

  async submit () {
    this.meetingRoom.updateMeetingRoom(
      new this.meetingRoom.MeetingRoom(
        this.params.id,
        this.$scope.form.displayName,
        this.$scope.form.number,
        this.$scope.form.userPin,
      )
    ).then(() => {
      this.$window.location.href = '?act=list';
    }, (error) => {
      this.onError(error, 'fm_mgt_error');
    });
  }

  async init () {
    this.$scope.form = {};
    this.params = this.parseParams(this.$window.location.search);
    this.meetingRoom.getMeetingRoom(this.params.id).then((result) => {
      this.$scope.form.displayName = result.data.displayName;
      this.$scope.form.number = result.data.number;
      this.$scope.form.userPin = result.data.userPin || "";
    });
  }

  checkNumberAvailability (form) {
    this.checkAvailability('number', form, this.meetingRoom.numberIsAvailable);
  }

  checkDisplayNameAvailability (form) {
    this.checkAvailability('displayName', form, this.meetingRoom.displayNameIsAvailable);
  }

  checkAvailability (name, form, fn) {
    if (this.$scope.form[name]) {
      this.isLoading = true;
      fn(this.$scope.form[name], this.params.id).then((result) => {
        form[name].$setValidity(`duplicate${name}`, result);
        this.isLoading = false;
      });
    }
  }
}

export default class IpbxMediaServerController {
  constructor($scope, mediaServers, $log) {
    this.mediaServers = mediaServers;
    this.$scope = $scope;
    this.$log = $log;


    this.$scope.list = [];
    this.$scope.headers = [
      {title: 'media_servers', dataKey: 'display_name'},
      {title: 'media_servers_line_count', dataKey: 'line_count'}
    ];

    this.$scope.path = '/xivo/configuration/index.php/manage/mediaserver/';


    this.$scope.isLoading = true;
    mediaServers.listMediaServersWithLines()
      .then((response) => {
        let cleanedlist = [];
        for (let mds of response.data) {
          mds.mediaServer.line_count = mds.line_count;
          cleanedlist.push(mds.mediaServer);
        }
        this.$scope.list = cleanedlist;
        this.$scope.isLoading = false;
        delete this.$scope.error;
      }, (error) => {
        this.$scope.error ="fm_generic_error";
        $log.error(error);
      });
  }
}

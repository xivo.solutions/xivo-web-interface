
export default function run($http){

  if(!document.cookie.split('; ').find(row => row.startsWith('XSRF-TOKEN'))) {
    $http({
      url: '/configmgt/api/2.0/healthcheck/running',
      method: 'GET'
    });
  }

}

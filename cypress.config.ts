import { defineConfig } from 'cypress'
import * as dotenv from 'dotenv';

const webpackPreprocessor = require('@cypress/webpack-preprocessor');

dotenv.config()

export default defineConfig({
  e2e: {
    specPattern: './cypress/tests',
    supportFile: './cypress/support/e2e.ts',
    baseUrl: process.env.BASE_URL || "http://localhost:8070",
  },
  setupNodeEvents(on, config) {
    on('file:preprocessor', webpackPreprocessor());
  }
})

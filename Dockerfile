FROM php:8.2-fpm-bullseye

WORKDIR /usr/share/xivo-web-interface

RUN apt-get update && apt-get install -y \
    gcc make autoconf libc-dev pkg-config \
    libpng-dev \
    libldap2-dev \
    libpq-dev \
    libpcre3-dev \
    libxml2-dev \
    libxslt-dev \
    libssl-dev \
    libssh-dev \
    librabbitmq-dev \
    librabbitmq4

RUN rm -rf /var/lib/apt/lists/*

# Ajout des extensions nécessaires
RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-install -j$(nproc) ldap \
    pcntl

# Autres extensions
RUN docker-php-ext-install -j$(nproc) bcmath \
    calendar \
    exif \
    gd \
    gettext \
    pgsql \
    pdo_pgsql \
    shmop \
    sockets \
    sysvmsg \
    sysvsem \
    sysvshm \
    xsl \
    opcache \
    xml

# Installer et activer l'extension AMQP
RUN pecl install amqp-2.1.2 && \
    docker-php-ext-enable amqp

RUN curl -fsSL 'https://github.com/php-amqplib/php-amqplib/archive/v2.9.2.tar.gz' -o php-amqplib.tar.gz && \
    mkdir /tmp/php-amqplib

RUN tar -xf php-amqplib.tar.gz -C /tmp/php-amqplib --strip-components=1 && \
    rm php-amqplib.tar.gz && \
    mkdir -p /usr/share/php/ && \
    ln -s /tmp/php-amqplib/PhpAmqpLib/ /usr/share/php/

RUN mkdir /etc/xivo && \
    mkdir /etc/xivo/web-interface && \
    mkdir /var/log/xivo-web-interface && \
    touch /var/log/xivo-web-interface/xivo.log

COPY docker/xivo.ini /etc/xivo/web-interface/xivo.ini
COPY docker/php.ini /etc/xivo/web-interface/php.ini
COPY docker/ipbx.ini /etc/xivo/web-interface/ipbx.ini
COPY docker/monitoring.ini /etc/xivo/web-interface/monitoring.ini
COPY docker/user.ini /etc/xivo/web-interface/user.ini
COPY src /usr/share/xivo-web-interface
COPY docker/zzzxivo.conf /usr/local/etc/php-fpm.d/

RUN ln -s /etc/xivo/web-interface/php.ini /usr/local/etc/php/conf.d/xivo.ini

# Mount and symlink for the munin graphs
RUN mkdir -p /var/www/munin/localdomain/localhost.localdomain && \
    mkdir -p /usr/share/xivo-web-interface/www/img/graphs/munin/localdomain/ && \
    ln -s /var/www/munin/localdomain/localhost.localdomain/ /usr/share/xivo-web-interface/www/img/graphs/munin/localdomain/

# Version
ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

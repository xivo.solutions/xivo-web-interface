import { common } from "@helpers/common";

describe("password securisation process Test", () => {
  beforeEach(() => {
    common.initialSate();
  });

  describe("cti test", () => {
    const goToCtiPage = () => {
      cy.visit(
        "/service/ipbx/index.php/pbx_settings/users/?act=add"
      );
      cy.wait(3000);
      cy.get("#it-userfeatures-enableclient").should("exist").click();
    };

    it("Should generate a password", () => {
      goToCtiPage();

      cy.get(".icon-reset").click({ force: true });
      cy.get("#it-userfeatures-passwdclient")
        .invoke("val")
        .should(
          "match",
          /^(?=.*[A-Z])(?=.*[a-z])(?=.*?[0-9])[A-Za-z0-9\W_][^\"\\\'\<\>]{8,}$/
        );
    });

    it("Should show/hide password", () => {
      goToCtiPage();

      cy.get("#it-userfeatures-passwdclient")
        .invoke("attr", "type")
        .should("equal", "password");

      cy.get(".icon-hide").should("exist").click();

      cy.get(".icon-eye").should("exist").click();
    });
  });
});

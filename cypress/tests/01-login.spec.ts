import { common } from "@helpers/common";

describe("Login process Test", () => {
  beforeEach(() => {
    cy.visit("");
  });

  it("Should Login!", () => {
    common.login("root", "superpass");
    cy.url().should("include", "/xivo/index.php");
  });

  it("Should not Login! - wrong login", () => {
    common.login("root2", "superpass");
    cy.url().should("not.include", "/xivo/index.php");
  });

  it("Should not Login! - wrong password", () => {
    common.login("root", "superpassZ");
    cy.url().should("not.include", "/xivo/index.php");
  });
});

import { common } from "@helpers/common";

describe("webservice test", () => {
  beforeEach(() => {
    common.initialSate();
  });

  const goToWebServicePage = () => {
    cy.visit(
      "/xivo/configuration/index.php/manage/accesswebservice/?act=add"
    );
    cy.wait(3000);
    cy.get("#it-passwd").should("exist");
  };

  it("Should generate a password", () => {
    goToWebServicePage();

    cy.get(".icon-reset").click({ force: true });
    cy.get("#it-passwd")
      .invoke("val")
      .should(
        "match",
        /^(?=.*[A-Z])(?=.*[a-z])(?=.*?[0-9])(?=.*[\W_])[A-Za-z0-9\W_][^\"\\\'\<\>]{14,}$/
      );
  });

  it("Should show/hide password", () => {
    goToWebServicePage();

    cy.get("#it-passwd").invoke("attr", "type").should("equal", "password");

    cy.get(".icon-hide").should("exist").click();

    cy.get("#it-passwd").invoke("attr", "type").should("equal", "text");

    cy.get(".icon-eye").should("exist").click();
  });
});

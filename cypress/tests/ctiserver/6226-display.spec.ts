import { common } from "@helpers/common";

describe("webservice test", () => {
  beforeEach(() => {
    cy.viewport(1920, 1080)
    common.initialSate();
  });

  const goToDisplayPage = () => {
    cy.visit(
      "/cti/index.php/displays"
    );
  };

  it("Should contain the default display", () => {
    goToDisplayPage();
    const defaultDisplayLine = cy.get('#table-main-listing tr')
    .filter(':contains("Display")')
    .filter(':contains("Affichage par défaut")')
    .should('exist');

    defaultDisplayLine.find('a[title="Edit"]').should('exist');
    defaultDisplayLine.find('a[title="Delete"]').should('not.exist');
  });

  it("Should be able to edit labels for the default display", () => {
    goToDisplayPage();
    cy.get('#table-main-listing tr')
    .filter(':contains("Display")')
    .filter(':contains("Affichage par défaut")')
    .find('a[title="Edit"]').click();
    cy.get('input[id="dispcol1[]"][value="Pseudonyme"]')
    .should('not.exist');
    cy.get('input[id="dispcol1[]"][value="Subtitle1"]')
    .should('exist')
    .clear()
    .type('Pseudonyme');
    cy.get('#it-submit').click();

    cy.get('#table-main-listing tr')
    .filter(':contains("Display")')
    .filter(':contains("Affichage par défaut")')
    .find('a[title="Edit"]').click();
    cy.get('input[id="dispcol1[]"][value="Subtitle1"]')
    .should('not.exist');
    cy.get('input[id="dispcol1[]"][value="Pseudonyme"]')
    .should('exist')
    .clear()
    .type('Subtitle1');
    cy.get('#it-submit').click();    

  });

  it("Should be able to create a new display", () => {
    goToDisplayPage();
    cy.get('a[href^="./?act=add"]').should('exist');
    cy.visit("/cti/index.php/displays/?act=add")    
    cy.get('#it-displays-name').type('Displate');
    cy.get('#display-filter-add').click();
    cy.get('input[id="dispcol1[]"]').type('Nom');
    cy.get('input[id="dispcol2[]"]').type('name');
    cy.get('input[id="dispcol3[]"]').type('Tony');
    cy.get('input[id="dispcol4[]"]').type('header_1_info');
    cy.get('#it-submit').click();

    cy.get('#table-main-listing tr')
    .filter(':contains("Displate")')
    .find('a[title="Edit"]').click();
    cy.get('input[id="dispcol1[]"][value="Nom"]')
    .should('exist');
    cy.get('a[title="Remove this field"]').click();
    cy.get('#it-submit').click();

    cy.get('#table-main-listing tr')
    .filter(':contains("Displate")')
    .find('a[title="Edit"]').click();
    cy.get('input[id="dispcol1[]"][value="Nom"]')
    .should('not.exist');
    cy.get('#display-filter-add').click();
    cy.get('input[id="dispcol1[]"]').type('Mail');
    cy.get('input[id="dispcol2[]"]').type('email');
    cy.get('input[id="dispcol4[]"]').type('contact_5_email');
    cy.get('#it-submit').click();

    cy.get('#table-main-listing tr')
    .filter(':contains("Displate")')
    .find('a[title="Delete"]').click();
  
  });




});

import { Form } from "cypress-helper";

export class Common {

    userToken: string = '';

    login(username: string, password: string): void {
        const loginForm: Form = {
          fields: {
            username: { selector: "#it-login", value: username },
            password: { selector: "#it-password", value: password },
          },
          submitButton: "#it-submit",
        };
        cy.fillForm(loginForm);
    }

    initialSate() {
        cy.visit('');
        this.login('root', 'superpass');
        cy.wait(3000);
        cy.url().should('include', '/xivo/index.php');
    }
}

export const common = new Common();
